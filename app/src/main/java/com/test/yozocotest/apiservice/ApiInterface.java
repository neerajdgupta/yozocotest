package com.test.yozocotest.apiservice;

import com.test.yozocotest.model.Product;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {

    @GET("APILIST")
    Call<List<Product>> getListFromApi();
}
