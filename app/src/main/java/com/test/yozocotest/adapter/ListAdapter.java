package com.test.yozocotest.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.test.yozocotest.R;
import com.test.yozocotest.databinding.ItemRowLayoutBinding;
import com.test.yozocotest.model.Product;

import java.util.ArrayList;
import java.util.List;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ListViewHolder> {

    private List<Product> apiResponses;
    private ListListener listListener;

    public ListAdapter(ListListener listListener) {
       this.listListener= listListener;
    }

    public interface ListListener{
        void onClick(Product product);
    }


    public void setList(List<Product> apiResponses){
        this.apiResponses=apiResponses;
    }

    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemRowLayoutBinding itemRowLayoutBinding= DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_row_layout,parent,false);
        return new ListViewHolder(itemRowLayoutBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ListViewHolder holder, final int position) {
          final Product apiResponse=apiResponses.get(position);
          if(apiResponse!=null){
              Glide.with(holder.binding.imgView).load(apiResponse.getImage()).into(holder.binding.imgView);
              holder.binding.tvProductName.setText(apiResponse.getProductName());
              holder.binding.tvProductCompany.setText(apiResponse.getCompany());
              holder.binding.tvProductPrice.setText(apiResponse.getPrice());
          }

          holder.binding.getRoot().setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View view) {
                  listListener.onClick(apiResponses.get(position));
              }
          });

    }

    @Override
    public int getItemCount() {
        return apiResponses==null?0:apiResponses.size();
    }

    class ListViewHolder extends RecyclerView.ViewHolder {

        ItemRowLayoutBinding binding;
        public ListViewHolder(@NonNull ItemRowLayoutBinding binding ) {
            super(binding.getRoot());
            this.binding=binding;
        }
    }
}
