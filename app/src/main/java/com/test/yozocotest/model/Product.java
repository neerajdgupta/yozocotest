package com.test.yozocotest.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "product_table")
public class Product {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "insertId")
    private int insertId;

    @ColumnInfo(name = "product_name")
    @SerializedName("product_name")
    @Expose
    private String productName;

    @ColumnInfo(name = "price")
    @SerializedName("price")
    @Expose
    private String price;

    @ColumnInfo(name = "company")
    @SerializedName("company")
    @Expose
    private String company;

    @ColumnInfo(name = "image")
    @SerializedName("image")
    @Expose
    private String image;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getInsertId() {
        return insertId;
    }

    public void setInsertId(int insertId) {
        this.insertId = insertId;
    }
}


