package com.test.yozocotest.view.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import com.test.yozocotest.R;
import com.test.yozocotest.adapter.ViewPagerAdapter;
import com.test.yozocotest.databinding.ActivityMainBinding;
import com.test.yozocotest.model.Product;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    public ActivityMainBinding binding;
    private MainViewModel mainViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

       binding= DataBindingUtil.setContentView(this, R.layout.activity_main);
        ViewPagerAdapter viewPagerAdapter=new ViewPagerAdapter(getSupportFragmentManager(),0);
        binding.viewPager.setAdapter(viewPagerAdapter);
        binding.tablayout.setupWithViewPager(binding.viewPager);

        init();



    }

    private void init() {

        mainViewModel= ViewModelProviders.of(MainActivity.this).get(MainViewModel.class);

        mainViewModel.getGetAllProduct().observe(this, new Observer<List<Product>>() {
            @Override
            public void onChanged(List<Product> products) {

            }
        });

    }
}
