package com.test.yozocotest.view.fragments;


import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.ListFragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.test.yozocotest.R;
import com.test.yozocotest.adapter.ListAdapter;
import com.test.yozocotest.databinding.FragmentListBinding;
import com.test.yozocotest.model.Product;
import com.test.yozocotest.view.activity.MainActivity;
import com.test.yozocotest.view.activity.MainViewModel;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListViewFragment extends Fragment implements ListAdapter.ListListener  {


    private FragmentListBinding fragmentListBinding;
    private MainViewModel mainViewModel;
    private ListAdapter listAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        fragmentListBinding= DataBindingUtil.inflate(inflater,R.layout.fragment_list, container, false);
        initViewModel();
        return fragmentListBinding.getRoot();
    }

    private void initViewModel() {
        if(getActivity()!=null)
        mainViewModel= ViewModelProviders.of(getActivity()).get(MainViewModel.class);

        listAdapter=new ListAdapter(this);
        mainViewModel.getGetAllProduct().observe(this, new Observer<List<Product>>() {
            @Override
            public void onChanged(List<Product> products) {
                fragmentListBinding.rvList.setLayoutManager(new LinearLayoutManager(getActivity(),RecyclerView.VERTICAL,false));
                fragmentListBinding.rvList.setHasFixedSize(false);
                fragmentListBinding.rvList.setAdapter(listAdapter);
                listAdapter.setList(products);
            }
        });
    }

    @Override
    public void onClick(Product product) {
        mainViewModel.getProductMutableLiveData().setValue(product);
        if(getActivity()!=null)
        ((MainActivity)getActivity()).binding.viewPager.setCurrentItem(1,true);
    }
}
