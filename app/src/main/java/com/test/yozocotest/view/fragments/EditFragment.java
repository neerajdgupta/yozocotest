package com.test.yozocotest.view.fragments;


import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.test.yozocotest.R;
import com.test.yozocotest.databinding.FragmentEditBinding;
import com.test.yozocotest.databinding.FragmentListBinding;
import com.test.yozocotest.model.Product;
import com.test.yozocotest.view.activity.MainActivity;
import com.test.yozocotest.view.activity.MainViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditFragment extends Fragment {

    private FragmentEditBinding fragmentEditBinding;
   private MainViewModel mainViewModel;
   private Product pt;

    public EditFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        fragmentEditBinding= DataBindingUtil.inflate(inflater,R.layout.fragment_edit, container, false);
        setViewModel();
        return fragmentEditBinding.getRoot();
    }

    private void setViewModel() {
        if(getActivity()!=null)
        mainViewModel= ViewModelProviders.of(getActivity()).get(MainViewModel.class);

        mainViewModel.getProductMutableLiveData().observe(this, new Observer<Product>() {
            @Override
            public void onChanged(Product product) {
                pt=product;
                Glide.with(fragmentEditBinding.imgView).load(product.getImage()).into(fragmentEditBinding.imgView);
                fragmentEditBinding.tvProductCompany.setText(product.getCompany());
                fragmentEditBinding.tvProductName.setText(product.getProductName());
                fragmentEditBinding.etProductPrice.setText(product.getPrice());

            }
        });

        fragmentEditBinding.btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(pt!=null){
                    pt.setPrice(fragmentEditBinding.etProductPrice.getText().toString());
                mainViewModel.update(pt);

                    if(getActivity()!=null)
                        ((MainActivity)getActivity()).binding.viewPager.setCurrentItem(0,true);

                }
            }
        });



    }

}
