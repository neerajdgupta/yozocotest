package com.test.yozocotest.view.activity;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.test.yozocotest.apiservice.ApiInterface;
import com.test.yozocotest.model.Product;
import com.test.yozocotest.network.ApiClient;
import com.test.yozocotest.room.ProductRepository;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainViewModel extends AndroidViewModel {




    private LiveData<List<Product>> getAllProduct;

    private ProductRepository productRepository;


    private MutableLiveData<Product> productMutableLiveData;

    public MainViewModel(@NonNull Application application) {
        super(application);
        productRepository=new ProductRepository(application);
        getAllProduct=productRepository.getAllProduct();
        callApi();

    }


    private void callApi() {
        ApiClient.getClient().create(ApiInterface.class).getListFromApi().enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {

                if (response.body() != null ) {
                    insert(response.body());
                }


            }

            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {
                Toast.makeText(getApplication(), "Fail", Toast.LENGTH_SHORT).show();
            }
        });
    }





    public void insert(List<Product> products) {
        productRepository.insertProduct(products);
    }

    public void update(Product product){
        productRepository.updateModel(product);
    }


    public LiveData<List<Product>> getGetAllProduct() {
        return getAllProduct;
    }



    public MutableLiveData<Product> getProductMutableLiveData() {
        if(productMutableLiveData==null){
            productMutableLiveData=new MutableLiveData<>();
        }
        return productMutableLiveData;
    }


}
