package com.test.yozocotest.room;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.test.yozocotest.model.Product;

import java.util.List;

@Dao
public interface ProductDao {


    @Insert
     void insert(List<Product> products);

    @Query("DELETE FROM product_table")
    void deleteAll();

    @Query("SELECT * from product_table")
    LiveData<List<Product>> getAllProduct();

    @Update
    void update(Product product);



}
