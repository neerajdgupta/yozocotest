package com.test.yozocotest.room;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.test.yozocotest.apiservice.ApiInterface;
import com.test.yozocotest.model.Product;
import com.test.yozocotest.network.ApiClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductRepository {

   private Application application;
   private ProductDao productDao;
   private LiveData<List<Product>> productList;

    public ProductRepository(Application application) {

        DataBase roomDatabase=DataBase.getDatabase(application);
        productDao=roomDatabase.productDao();
        productList=productDao.getAllProduct();
        this.application = application;
    }



    public LiveData<List<Product>> getAllProduct(){
       return productList;
    }

   public void insertProduct(List<Product> products){
        new insertAsyncTask(productDao).execute(products);
    }

    private static class insertAsyncTask extends AsyncTask<List<Product>,Void ,Void > {

        private ProductDao productDao;

        insertAsyncTask(ProductDao productDao) {
            this.productDao = productDao;
        }

        @SafeVarargs
        @Override
        protected final Void doInBackground(List<Product>... lists) {
            productDao.deleteAll();
            productDao.insert(lists[0]);
            return null;
        }


    }


    public void updateModel(Product product){
        new UpdateAsyncTask(productDao).execute(product);
    }

    private static class UpdateAsyncTask extends AsyncTask<Product,Void,Void>{

        private ProductDao productDao;

        UpdateAsyncTask(ProductDao productDao) {
            this.productDao = productDao;
        }


        @Override
        protected Void doInBackground(Product... products) {
            productDao.update(products[0]);
            return null;
        }

    }





}
