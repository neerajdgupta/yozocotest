package com.test.yozocotest.room;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.test.yozocotest.model.Product;

@Database(entities = {Product.class},version = 1)
public abstract class DataBase extends RoomDatabase {

    public abstract ProductDao productDao();

    private static volatile DataBase INSTANCE;

    static DataBase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (DataBase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            DataBase.class, "room_database")
                            .build();
                }
            }
        }
        return INSTANCE;
    }

}
